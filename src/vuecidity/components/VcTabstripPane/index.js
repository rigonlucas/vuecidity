import VcTabstripPane from './VcTabstripPane'

VcTabstripPane.install = function install (Vue) {
  Vue.component(VcTabstripPane.name, VcTabstripPane)
}

export default VcTabstripPane
