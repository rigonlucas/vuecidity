import VcButtonSplit from './VcButtonSplit'

VcButtonSplit.install = function install (Vue) {
  Vue.component(VcButtonSplit.name, VcButtonSplit)
}

export default VcButtonSplit
