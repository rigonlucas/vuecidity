import VcBreadcrumbs from './VcBreadcrumbs'

VcBreadcrumbs.install = function install (Vue) {
  Vue.component(VcBreadcrumbs.name, VcBreadcrumbs)
}

export default VcBreadcrumbs
