import VcListItem from './VcListItem'

VcListItem.install = function install (Vue) {
  Vue.component(VcListItem.name, VcListItem)
}

export default VcListItem
