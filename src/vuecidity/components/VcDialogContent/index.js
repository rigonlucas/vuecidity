import VcDialogContent from './VcDialogContent'

VcDialogContent.install = function install (Vue) {
  Vue.component(VcDialogContent.name, VcDialogContent)
}

export default VcDialogContent
