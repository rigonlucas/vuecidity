import VcAvatar from './VcAvatar'

VcAvatar.install = function install (Vue) {
  Vue.component(VcAvatar.name, VcAvatar)
}

export default VcAvatar
