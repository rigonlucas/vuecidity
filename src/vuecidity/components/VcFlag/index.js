import VcFlag from './VcFlag'

VcFlag.install = function install (Vue) {
  Vue.component(VcFlag.name, VcFlag)
}

export default VcFlag
