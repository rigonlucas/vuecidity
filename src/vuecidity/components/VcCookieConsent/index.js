import VcCookieConsent from './VcCookieConsent'

VcCookieConsent.install = function install (Vue) {
  Vue.component(VcCookieConsent.name, VcCookieConsent)
}

export default VcCookieConsent
