import VcText from './VcText'

VcText.install = function install (Vue) {
  Vue.component(VcText.name, VcText)
}

export default VcText
