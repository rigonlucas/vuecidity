import VcButtonGroup from './VcButtonGroup'

VcButtonGroup.install = function install (Vue) {
  Vue.component(VcButtonGroup.name, VcButtonGroup)
}

export default VcButtonGroup
