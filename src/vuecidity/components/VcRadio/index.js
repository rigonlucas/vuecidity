import VcRadio from './VcRadio'

VcRadio.install = function install (Vue) {
  Vue.component(VcRadio.name, VcRadio)
}

export default VcRadio
