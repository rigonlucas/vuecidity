import VcLoadingOverlay from './VcLoadingOverlay'

VcLoadingOverlay.install = function install (Vue) {
  Vue.component(VcLoadingOverlay.name, VcLoadingOverlay)
}

export default VcLoadingOverlay
