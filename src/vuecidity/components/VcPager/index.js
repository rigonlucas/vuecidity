import VcPager from './VcPager'

VcPager.install = function install (Vue) {
  Vue.component(VcPager.name, VcPager)
}

export default VcPager
