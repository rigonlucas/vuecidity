import VcCard from './VcCard'

VcCard.install = function install (Vue) {
  Vue.component(VcCard.name, VcCard)
}

export default VcCard
