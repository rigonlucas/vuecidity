import Ripple from '../../directives/ripple.js'
import Tooltip from '../../directives/tooltip.js'
import Resize from '../../directives/resize.js'

Ripple.color = 'rgba(255, 255, 255, 0.25)'
Ripple.zIndex = 55

const Vuecidity = {
  install (Vue, opts = {}) {
    if (this.installed) return

    this.installed = true

    const $vuecidity = {}
    Vue.util.defineReactive($vuecidity, 'inspire', {})

    Vue.prototype.$vuecidity = $vuecidity.inspire

    Vue.directive('ripple', Ripple)
    Vue.directive('tooltip', Tooltip)
    Vue.directive('resize', Resize)

    if (opts.transitions) {
      Object.keys(opts.transitions).forEach(key => {
        const t = opts.transitions[key]
        if (t.name !== undefined && t.name.startsWith('vc-')) {
          Vue.component(t.name, t)
        }
      })
    }

    if (opts.directives) {
      Object.keys(opts.directives).forEach(key => {
        const d = opts.directives[key]
        Vue.directive(d.name, d)
      })
    }

    if (opts.components) {
      Object.keys(opts.components).forEach(key => {
        const c = opts.components[key]
        Vue.use(c)
      })
    }
  }
}

export default Vuecidity
