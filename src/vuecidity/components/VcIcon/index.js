import VcIcon from './VcIcon'

VcIcon.install = function install (Vue) {
  Vue.component(VcIcon.name, VcIcon)
}

export default VcIcon
