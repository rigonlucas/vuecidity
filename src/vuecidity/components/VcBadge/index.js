import VcBadge from './VcBadge'

VcBadge.install = function install (Vue) {
  Vue.component(VcBadge.name, VcBadge)
}

export default VcBadge
